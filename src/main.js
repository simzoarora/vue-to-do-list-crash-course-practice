import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import firebase from 'firebase'


// Required for side-effects
require("firebase/firestore");
// var firebaseConfig = {
//   apiKey: "AIzaSyC6WG0-aND_V5tOk0dj2URmhdJKu6a7hWY",
//   authDomain: "messaging-9bffe.firebaseapp.com",
//   projectId: "messaging-9bffe",
//   storageBucket: "messaging-9bffe.appspot.com",
//   messagingSenderId: "646637940008",
//   appId: "1:646637940008:web:8af11b997c7d796416741d",
//   measurementId: "G-REL3GFPNN2"
// };

  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyC_HGTn_4zgCUqRNCen5sDzF_8jzD-UPbM",
    authDomain: "nuway-401052.firebaseapp.com",
    databaseURL: "https://nuway-401052.firebaseio.com", 
    projectId: "nuway-401052",
    storageBucket: "nuway-401052.appspot.com",
    messagingSenderId: "797101440217",
    appId: "1:797101440217:web:5f2197ec3437a111ead2c8"
    };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  var db = firebase.firestore();

  window.db = db;


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
